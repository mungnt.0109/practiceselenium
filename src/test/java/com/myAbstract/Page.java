package com.myAbstract;

import org.openqa.selenium.By;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;

public abstract class Page {

	WebDriver driver;
	WebDriverWait wait;

	// constructor
	public Page(WebDriver driver) {
		this.driver = driver;
		this.wait = new WebDriverWait(this.driver, 15);
	}

	public abstract WebElement getElement(By locator);

	public abstract void waitForElementPresent(By locator);

	public abstract void waitForPageLoad();
	
	public abstract String getTitle();

	public <TPage extends BasePage> TPage getInstance(Class<TPage> pageClass) {
		TPage T = null;
		try {
			
			//getDeclaredConstructor (khởi tạo cái pageClass) với biến truyền vào là driver
			//TPage tPage = new TPage(WebDriver);
			T = pageClass.getDeclaredConstructor(WebDriver.class).newInstance(this.driver);
			return  T;
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return T;
	}

}
